class Donation < ActiveRecord::Base
  belongs_to :donor
  validates_presence_of :amount, :date
  validates_numericality_of :amount, :greater_than_or_equal_to => 0.01

  validate :date_donation_cannot_be_in_the_future

  def date_donation_cannot_be_in_the_future
    errors.add(:date, "can't be in the future") if !date.blank? and date > Date.today
  end

end
